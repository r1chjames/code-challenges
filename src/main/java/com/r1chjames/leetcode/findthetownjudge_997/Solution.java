package com.r1chjames.leetcode.findthetownjudge_997;

import java.util.*;

class Solution {
    public int findJudge(int n, int[][] trust) {
        Map<Integer, Integer> trusted = new HashMap<>();

        for (int[] ints : trust) {
            trusted.putIfAbsent(ints[0], 0);
            trusted.putIfAbsent(ints[1], 0);
            trusted.compute(ints[0], (k, v) -> v - 1);
            trusted.compute(ints[1], (k, v) -> v + 1);
        }

        var a = trusted
                .entrySet()
                .stream()
                .filter(v -> v.getValue() == n - 1)
                .findFirst();

        return (a.isPresent()) ? a.get().getKey() : -1;
    }
}
