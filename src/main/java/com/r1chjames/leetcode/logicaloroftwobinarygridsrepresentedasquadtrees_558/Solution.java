package com.r1chjames.leetcode.logicaloroftwobinarygridsrepresentedasquadtrees_558;

class Solution {
    public Node intersect(Node quadTree1, Node quadTree2) {
        if (quadTree1.isLeaf) {
            return quadTree1.val ? quadTree1 : quadTree2;
        } else if (quadTree2.isLeaf) {
            return quadTree2.val ? quadTree2 : quadTree1;
        } else {
            Node intersection = quadTree1;
            intersection.topLeft = intersect(quadTree1.topLeft, quadTree2.topLeft);
            intersection.topRight = intersect(quadTree1.topRight, quadTree2.topRight);
            intersection.bottomLeft = intersect(quadTree1.bottomLeft, quadTree2.bottomLeft);
            intersection.bottomRight = intersect(quadTree1.bottomRight, quadTree2.bottomRight);

            if (intersection.topLeft.isLeaf && intersection.topRight.isLeaf &&
                intersection.bottomLeft.isLeaf && intersection.bottomRight.isLeaf &&
                intersection.topLeft.val == intersection.topRight.val &&
                intersection.topLeft.val == intersection.bottomLeft.val &&
                intersection.topLeft.val == intersection.bottomRight.val) {
                intersection.isLeaf = true;
                intersection.val = true;
                intersection.topLeft = null;
                intersection.topRight = null;
                intersection.bottomLeft = null;
                intersection.bottomRight = null;
            }
            return intersection;
        }
    }
}


// Definition for a QuadTree node.
class Node {
    public boolean val;
    public boolean isLeaf;
    public Node topLeft;
    public Node topRight;
    public Node bottomLeft;
    public Node bottomRight;

    public Node() {}

    public Node(boolean val, boolean isLeaf, Node topLeft, Node topRight, Node bottomLeft, Node bottomRight) {
        this.val = val;
        this.isLeaf = isLeaf;
        this.topLeft = topLeft;
        this.topRight = topRight;
        this.bottomLeft = bottomLeft;
        this.bottomRight = bottomRight;
    }
}