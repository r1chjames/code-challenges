package com.r1chjames.leetcode.reversewordsinastring3_557;

class Solution {

    protected String reverseWords(String str) {
        var strArr = str.split(" ");
        StringBuilder outputStr = new StringBuilder();

        for (var word : strArr) {
            var wordArr = word.toCharArray();
            var op = "";
            for (int i = wordArr.length - 1; i >= 0; i--) {
                op += wordArr[i];
            }
            outputStr.append(String.format(" %s", op));
        }

        return outputStr.toString().trim();
    }
}