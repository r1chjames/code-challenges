package com.r1chjames.leetcode.nextgreaterelement3_556;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {

    private Solution solution = new Solution();

    @Test
    void Test12Returns21() {
        assertEquals(21, solution.nextGreaterElement(12));
    }

    @Test
    void Test21ReturnsNeg1() {
        assertEquals(-1, solution.nextGreaterElement(21));
    }

    @Test
    void Test121Returns211() {
        assertEquals(211, solution.nextGreaterElement(121));
    }

    @Test
    void Test43388Returns43838() {
        assertEquals(43838, solution.nextGreaterElement(43388));
    }

}