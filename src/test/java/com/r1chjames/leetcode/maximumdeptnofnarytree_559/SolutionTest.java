package com.r1chjames.leetcode.maximumdeptnofnarytree_559;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SolutionTest {

    com.r1chjames.leetcode.maximumdeptnofnarytree_559.Solution solution = new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Solution();

    @Test
    void TestExample1() {
        com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node node =
                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(1,
                        List.of(
                                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(3,List.of(
                                        new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(5, null),
                                        new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(6, null))
                                ),
                                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(2, null),
                                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(4, null)
                        )
                );

        assertEquals(3, solution.maxDepth(node));
    }

    @Test
    void TestExample2() {
        com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node node =
                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(1,
                        List.of(
                                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(2, null),
                                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(3,List.of(
                                        new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(6, null),
                                        new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(7,
                                                List.of(
                                                        new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(11,
                                                                List.of(new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(14, null)
                                                )))
                                ))),
                                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(4, List.of(
                                        new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(8, List.of(
                                                new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(12, null)
                                        )))
                                ),
                                 new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(5, List.of(
                                         new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(9, List.of(
                                                 new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(13, null)
                                         )),
                                         new com.r1chjames.leetcode.maximumdeptnofnarytree_559.Node(10, null)
                                 ))
                                )
                );

        assertEquals(5, solution.maxDepth(node));
    }

}