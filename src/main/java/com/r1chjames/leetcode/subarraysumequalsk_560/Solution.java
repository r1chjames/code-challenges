package com.r1chjames.leetcode.subarraysumequalsk_560;

import java.util.HashMap;
import java.util.Map;

class Solution {

    public int subarraySum(int[] nums, int k) {
        Map<Integer, Integer> sums = new HashMap<>();
        int sum = 0;
        sums.put(sum, 1);

        int count = 0;
        for (int num: nums) {
            sum += num;
            if (sums.containsKey(sum - k)) {
                count += sums.get(sum - k);
            }

            sums.compute(sum, (key, value) -> value == null ? 1 : value + 1);
        }
        return count;
    }
}