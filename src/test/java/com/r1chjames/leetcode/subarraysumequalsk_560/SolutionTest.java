package com.r1chjames.leetcode.subarraysumequalsk_560;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SolutionTest {

    Solution solution = new Solution();

    @Test
    void testExample1() {
        int[] nums = {1, 1, 1};
        int k = 2;

        assertEquals(2, solution.subarraySum(nums, k));
    }

    @Test
    void testExample2() {
        int[] nums = {1, 2, 3};
        int k = 3;

        assertEquals(2, solution.subarraySum(nums, k));
    }

    @Test
    void testExample3() {
        int[] nums = {4, 2, 3, 7, 3, 7, 4, 6};
        int k = 10;

        assertEquals(4, solution.subarraySum(nums, k));
    }

}