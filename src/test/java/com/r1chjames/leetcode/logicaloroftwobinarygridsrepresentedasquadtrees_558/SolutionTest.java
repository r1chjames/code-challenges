package com.r1chjames.leetcode.logicaloroftwobinarygridsrepresentedasquadtrees_558;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SolutionTest {

    private Solution solution = new Solution();

    @Test
    void testMatchingGrids() {
        Node node1 = new Node(
                true,
                false,
                new Node(true, true, null, null, null, null),
                new Node(true, true, null, null, null, null),
                new Node(false, true, null, null, null, null),
                new Node(false, true, null, null, null, null)
                );

        Node node2 = node1;

        Node expectedIntersection = new Node(
                true,
                false,
                new Node(true, true, null, null, null, null),
                new Node(true, true, null, null, null, null),
                new Node(false, true, null, null, null, null),
                new Node(false, true, null, null, null, null)
        );

        assertTrue(compareNode(expectedIntersection, solution.intersect(node1, node2)));
    }

    @Test
    void testNonMatchingGrids() {
        Node node1 = new Node(
                true,
                false,
                new Node(true, true, null, null, null, null),
                new Node(true, true, null, null, null, null),
                new Node(false, true, null, null, null, null),
                new Node(false, true, null, null, null, null)
                );

        Node node2 = new Node(
                true,
                false,
                new Node(true, true, null, null, null, null),
                new Node(true, false,
                        new Node(false, true, null, null, null, null),
                        new Node(false, true, null, null, null, null),
                        new Node(true, true, null, null, null, null),
                        new Node(true, true, null, null, null, null)),
                new Node(true, true, null, null, null, null),
                new Node(false, true, null, null, null, null)
        );

        Node expectedIntersection = new Node(
                true,
                false,
                new Node(true, true, null, null, null, null),
                new Node(true, true, null, null, null, null),
                new Node(true, true, null, null, null, null),
                new Node(false, true, null, null, null, null)
        );

        assertTrue(compareNode(expectedIntersection, solution.intersect(node1, node2)));
    }




    boolean compareNode(Node expected, Node actual) {
        if (expected == null && actual == null) {
            return true;
        } else if (expected == null || actual == null) {
            return false;
        } else {
            return expected.val == actual.val &&
                    expected.isLeaf == actual.isLeaf &&
                    compareNode(expected.topLeft, actual.topLeft) &&
                    compareNode(expected.topRight, actual.topRight) &&
                    compareNode(expected.bottomLeft, actual.bottomLeft) &&
                    compareNode(expected.bottomRight, actual.bottomRight);
        }
    }

}