package com.r1chjames.leetcode.reversewordsinastring3_557;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SolutionTest {

    private Solution solution = new Solution();

    @Test
    void TestHello() {
        assertEquals("olleh", solution.reverseWords("hello"));
    }

    @Test
    void TestHelloMyNameIs() {
        assertEquals("olleh ym eman si", solution.reverseWords("hello my name is"));
    }

    @Test
    void TestTheQuickBrownFox() {
        assertEquals("eht kciuq nworb xof depmuj revo eht yzal god", solution.reverseWords("the quick brown fox jumped over the lazy dog"));
    }
}