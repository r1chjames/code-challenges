package com.r1chjames.leetcode.findthetownjudge_997;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {

    Solution solution = new Solution();

    @Test
    void testExample1() {
        int n = 2;
        int[][] trust = {{1,2}};

        assertEquals(2, solution.findJudge(n, trust));
    }

    @Test
    void testExample2() {
        int n = 3;
        int[][] trust = {{1,3},{2,3}};

        assertEquals(3, solution.findJudge(n, trust));
    }

    @Test
    void testExample3() {
        int n = 3;
        int[][] trust = {{1,3},{2,3},{3,1}};

        assertEquals(-1, solution.findJudge(n, trust));
    }

    @Test
    void testExample4() {
        int n = 1;
        int[][] trust = {};
        assertEquals(1, solution.findJudge(n, trust));
    }

}